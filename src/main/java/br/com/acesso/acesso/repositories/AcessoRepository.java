package br.com.acesso.acesso.repositories;

import br.com.acesso.acesso.models.Acesso;
import org.springframework.data.repository.CrudRepository;

public interface AcessoRepository extends CrudRepository<Acesso, Long> {
    Acesso deleteByClienteIdAndPortaId(Long clientId, Long portaId);

    Acesso findByClienteIdAndPortaId(Long clientId, Long portaId);
}
