package br.com.acesso.acesso.clients;

import br.com.acesso.acesso.exceptions.ClienteNotFoundException;
import br.com.acesso.acesso.exceptions.PortaNotFoundException;
import feign.Response;
import feign.codec.ErrorDecoder;

public class PortaClientDecoder implements ErrorDecoder {

    private ErrorDecoder errorDecoder = new Default();

    @Override
    public Exception decode(String s, Response response) {
        if (response.status() == 404){
            throw new PortaNotFoundException();
        }else{
            return errorDecoder.decode(s, response);
        }
    }
}
