package br.com.acesso.acesso.clients;

import feign.codec.ErrorDecoder;
import org.springframework.context.annotation.Bean;

public class ClienteClientConfiguration {

    @Bean
    public ErrorDecoder getErrorDecoderCliente(){
        return new ClienteClientDecoder();
    }
}
