package br.com.acesso.acesso.clients;

import br.com.acesso.acesso.exceptions.PortaNotFoundException;
import feign.codec.ErrorDecoder;
import org.springframework.context.annotation.Bean;

public class PortaClientConfiguration {

    @Bean
    public ErrorDecoder getErrorDecoderPorta(){
        return new PortaClientDecoder();
    }

}
