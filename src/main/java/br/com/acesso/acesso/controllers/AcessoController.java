package br.com.acesso.acesso.controllers;

import br.com.acesso.acesso.models.Acesso;
import br.com.acesso.acesso.services.AcessoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/acesso")
public class AcessoController {

    @Autowired
    private AcessoService acessoService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Acesso createAcesso (@RequestBody @Valid Acesso acesso){

        Acesso acessoObjeto = acessoService.create(acesso);

        return acessoObjeto;
    }

    @DeleteMapping("/{clienteid}/{portaid}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteAcesso (@PathVariable(name = "clienteid") Long clienteId,
                              @PathVariable(name = "portaid") Long portaId){

        acessoService.delete(clienteId, portaId);
    }

    @GetMapping("/{clienteid}/{portaid}")
    public Acesso getAcesso (@PathVariable(name = "clienteid") Long clienteId,
                             @PathVariable(name = "portaid") Long portaId){

        Acesso acesso = acessoService.getAcessoByIds(clienteId, portaId);

        return acesso;
    }

}
