package br.com.acesso.acesso.services;

import br.com.acesso.acesso.clients.ClienteClient;
import br.com.acesso.acesso.clients.PortaClient;
import br.com.acesso.acesso.models.Acesso;
import br.com.acesso.acesso.models.Cliente;
import br.com.acesso.acesso.models.LogAcesso;
import br.com.acesso.acesso.models.Porta;
import br.com.acesso.acesso.repositories.AcessoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class AcessoService {

    @Autowired
    private AcessoRepository acessoRepository;

    @Autowired
    private PortaClient portaClient;

    @Autowired
    private ClienteClient clienteClient;

    @Autowired
    private KafkaTemplate<String, LogAcesso> producer;

    public Acesso create(Acesso acesso){

        Porta porta = portaClient.getPortaById(acesso.getPortaId());

        Cliente cliente = clienteClient.getClienteById(acesso.getClienteId());

        Acesso acessoObjeto = acessoRepository.save(acesso);

        return acessoObjeto;
    }

    public void delete(Long clientId, Long portaId){

        Acesso acesso = acessoRepository.deleteByClienteIdAndPortaId(clientId, portaId);

    }

    public Acesso getAcessoByIds(Long clientId, Long portaId){

        Acesso acesso = acessoRepository.findByClienteIdAndPortaId(clientId, portaId);

        LogAcesso logAcesso = logarAcesso(acesso, clientId, portaId);

        enviarAoKafka(logAcesso);

        return acesso;
    }

    private void enviarAoKafka(LogAcesso logAcesso) {
        producer.send("spec3-douglas-maldonado", logAcesso);
    }

    public LogAcesso logarAcesso(Acesso acesso, Long clientId, Long portaId){

        LogAcesso logAcesso = new LogAcesso();

        logAcesso.setDataHoraAcesso(LocalDateTime.now());

        if(acesso == null){
            logAcesso.setClienteId(clientId);
            logAcesso.setPortaId(portaId);
            logAcesso.setAcesso(false);
        }else{
            logAcesso.setClienteId(acesso.getClienteId());
            logAcesso.setPortaId(acesso.getPortaId());
            logAcesso.setAcesso(true);
        }

        return logAcesso;
    }
}
