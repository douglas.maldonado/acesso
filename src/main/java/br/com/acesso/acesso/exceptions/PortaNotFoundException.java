package br.com.acesso.acesso.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "Porta Não Encontrada")
public class PortaNotFoundException extends RuntimeException{
}
